export default {
    art1_title_:"Registration Application for successful candidates",
    art1_sent1_:"You will receive it from the Registration Application in Tertiary Education via the following link.",
    //Μηχανογραφικό;;;
    art1_sent2_:"Enter the 8-digit candidate exam code as your username and as your password, the same one you used to log in the application of the Mhxanografiko.",
    art1_sent3_:"During the Registration process, you must declare your contact information,",
    art1_sent4_:"your mobile phone number and your email address,",
    art1_sent5_:"which are confirmed through the relevant mechanisms of the application.",
    art1_sent6_:"It should be noted that the correctness of the contact details is highly important,",
    art1_sent7_:"as the Secretariats of the Departments will be able to use them in further procedures.",
    art2_title_:"Login to the UoM Register page (1)",
    art2_sent1_:"In the following link you will use the information: Candidate Code (you will find it on the card with which you took the exams but also in the registration form of the ministry).",
    art2_sent2_:"Candidate Initials (the 4 capital letters of your personal details in order: Surname, Name, Father's Name, Mother's Name) and SSN.",
    art2_sent3_:"After you log in, your details will appear.",  
    art2_sent4_:"ATTENTION: In the indication 'Academic Google platform' and specifically in the two fields marked in blue,",
    art2_sent5_:"your University email and the password are displayed.",
    art2_sent6_:"You must write them down (or print the whole page) as you will need them to complete the process and participate in the University's e-services!",
    art3_title_:"Login to the UoM Register page (2)",
    art3_sent1_:"Firstly, make sure you have all the required documents available in electronic form.",
    art3_sent2_:"Then, follow the instructions carefully in order to register.",
    art3_sent3_:"Check the website of your department. The required documents are usually the following:",
    art3_sent4_:"i. Copy of ID or Passport for foreign students (translated and certified by the public authorities).",
    art3_sent5_:"ii. Registration application (from the Registration Application in Tertiary Education [STEP 1]) signed by the candidate (you must print it and sign it at the bottom, anywhere you want).",
    art3_sent6_:"iii. Photo (file .jpg, with dimensions at least 240 x 240 px).",
    art3_sent7_:"Those of you who do not have one, you can take a photo with your mobile phone.",
    art3_sent8_:"To edit the photo you can use an image editing program such as Photoshop,",
    art3_sent9_:"GIMP or even online at the second link",
    art3_sent10_:"iv. Solemn Declaration of NON-registration in another School or Department of Tertiary Education.",
    art4_title_:"Login to the UoM Register page (3)",
    art4_sent1_:"You can print this declaration from the following link and sign it at a Citizen Service Center (KEP).",
    art4_sent2_:"Fill in your department, on the top of the document.",
    art4_sent3_:"Alternatively, you can do it through gov.gr as long as you have a bank account or a tax account in Greece.",
    art4_sent4_:"This way you do not need to visit the KEP.",
    //Όντας επίσημο κείμενο, δεν ξέρω αν αρκεί αυτη η μετάφραση ή χρειαζεται κάι συγκεκριμένος για το κάτω
    art4_sent5_:"Fill in the text: I am not registered in another Department or School of Tertiary Education",
    art4_sent6_:"ATTENTION: make sure the files you will send are correct before you send them. You cannot correct them afterwards.",
    art4_sent7_:"In the registration form (if the registrations are online and not in person) you will be asked for",
    art4_sent8_:"MALE REGISTRY NUMBER (for male students)",
    art4_sent9_:"MALE REGISTRY LOCATION (for male students)",
    art4_sent10_:"MUNICIPAL REGISTRY NUMBER (it is written on the back of your ID, in the field CITIZEN) *",
    art4_sent11_:"MUNICIPAL REGISTRY LOCATION (it is written on the back of your ID, in the field CITIZEN) *",
    art4_sent12_:"In the number fields, only numbers are accepted. Do not type the '/' in between, just the numbers.",
    art4_sent13_:"You will find all the information on your ID and on your high school diploma.",
    art5_title_:"Institutional Account",
    art5_sent1_:"Remember that you have two accounts (one email account & one for your login to e-services of the University).",
    art5_sent2_:"Both have a common username and a common initial password.",
    art5_sent3_:"Activate your institutional account in the first link below.",
    art5_sent4_:"Select 'Activate account now!', then 'I am a student' and then 'Begin activation'.",
    art5_sent5_:"ATTENTION: It is recommended for your convenience to choose the same password for the Institutional Account as the one set for the account in the Google Academic platform (G-Suite).",
    art5_sent6_:"To change the password in the 'UREGISTER service' you can log into the second link below.",
    art5_sent7_:"Remember, if you change the password you will have to use the new password to log in to all e-services,",
    art5_sent8_:"(except for Gmail, which is a separate service and you can manage it from the Gmail environment).",
    art6_title_:"Login/activation of academic email",
    art6_sent1_:"Visit the following link.",
    art6_sent2_:"Attention! If you have logged in to Gmail with your personal account,",
    art6_sent3_:"either log out to enter with the details they gave you, or you can open a private browsing tab to activate it there.",
    art7_title_:"Academic ID",
    art7_sent1_:"You can request an academic ID from the first link you will find in the relevant links.",
    art7_sent2_:"In the right column, at the top, there is a Register/Login button. Click it.",
    art7_sent3_:"In the login page, select undergraduate student and use the username of your uom.edu.gr email account and the password they provided you with.",
    art7_sent4_:"On the next screen there is a summary of what will be sent to the service: Academic ID.",
    art7_sent5_:"If you are OK with it, click Accept. If not, contact the Secretariat.", 
    art7_sent6_:"Read the terms of use and click 'I accept'.",
    art7_sent7_:"You will receive a confirmation email.",
    art7_sent8_:"Click on the link in the email to confirm your identity.",
    art7_sent9_:"Next, make sure that your details are correct. In the 'First Registration Date' field, select October and then upload your photo for the academic ID.",
    art7_sent10_:"Choose the pickup location for the physical copy of the ID.",
    art7_sent11_:"Don't forget to click 'Submit'. When you submit, you will receive a registration number.",
    art7_sent12_:"You should write it down. You will receive an email stating that the request has been successfully submitted.",   
    art7_sent13_:"You can request the academic ID, that also functions as a discount for public transport.",
    art7_sent14_:"You should also swap your phone number for a student number.",
    art7_sent15_:"Lastly, you should choose the subjects you want to attend on the StudentsWeb service, which you can find on the second link below.",
    rel_links_:"Related Links:",
    art8_title_:"Books from Eudoxus",
    art8_sent1_:"Visit the book distribution page 'Eudoxus',  which can be found in the first link.",
    art8_sent2_:"Select University of Macedonia and click confirm.",
    art8_sent3_:"Connect with your institutional account.",
    art8_sent4_:"Click on 'Books Selection' on the right.",
    art8_sent5_:"Select the current semester and click on 'Overview'.",
    art8_sent6_:"Select the books you are interested in and click on 'Continue'.",
    art8_sent7_:"Lastly, click on 'Final Submission.'",
    art8_sent8_:"For more information, you can visit the second link below.",
    art9_title_:"Library Registration",
    art9_sent1_:"Visit the first link.",
    art9_sent2_:"Select the 'New User Registration Form for University Members' or click on the second link.",
    art9_sent3_:"For the finalization of the registration, you will be needed to visit the library the next working day and in the span of a month, with your academic ID and your state issued ID, after booking an appointment.",
    art10_title_:"Open eClass Registration",
    art10_sent1_:"Visit the open eClass page, which can be found in the first link.",
    art10_sent2_:"Click on 'User Registration' on the left.",
    art10_sent3_:"Click on the 'Certification through the Institutional Account (URegister)'.",
    art10_sent4_:"Enter your username and password of your institutional account.",
    art10_sent5_:"On the next page, select your department and any other information that is required and click on submit.",
    art10_sent6_:"You will receive a confirmation email.",
    art10_sent7_:"Click on 'Subjects' on the left, select the 'Undergraduate' tab and then Department of Applied Informatics.",
    art10_sent8_:"Select the subjects of the semester you are enrolled in.",
    art10_sent9_:"It'd be a good idea to wait for the first lessons, as the professors will probably explain it more thoroughly.",
    art11_title_:"Boarding Information",
    art11_sent1_:"For students that reside in Thessaloniki, a boarding card is necessary.",
    art11_sent2_:"Firstly, you can find everything you need to know in the university's website, by clicking on the first link.",
    art11_sent3_:"An identity verification is required and afretwards, you can fill in the application for a boarding card, after the relevant announcement.",
    art11_sent4_:"Enter your username and password.",
    art11_sent5_:"In the related links, you can find documents and information needed for the application.",
    art12_title_:"Housing Information",
    art12_sent1_:"You can find an announcement for the housing application in the first link, for the academic year 2020-2021.",
    art12_sent2_:"In the related links, you can find documents and information needed for the application.",
    art12_sent3_:"An identity verification is required and afterwards, you can fill in the application.",
    art13_title_:"Health Care Information",
    art13_sent1_:"As a student, you are entitled to health care. You can find more information in the first link.",
    art13_sent2_:"The uninsured students are treated as uninsured citizens.",
    art13_sent3_:"The undergraduate, postgraduate students and PhD candidates that are not insured by other means",
    art13_sent4_:" are entitled to complete health care in the National Healthcare Service (E.S.Y.) with the expense coverage from ",
    art13_sent5_:"the National Organization for Healthcare Provision (E.O.P.Y.Y.) according to",
    art13_sent6_:"law 4368/2016 (Α΄ 147) article 31 paragraph 3.",
    art13_sent7_:"The University of Macedonia, beginning from 1/9/2017, stops",
    art13_sent8_:"providing healthcare to ",
    art13_sent9_:"uninsured students, and thus stops issuing and renewing health booklets.",
    art13_sent10_:"The uninsured students, with their SSN, can address to Public Health Centers (K.Y.)",
    art13_sent11_:"according to par. 1 of the article 33 of the law 4368/2016 (Α΄ 21).",
    art13_sent12_:"Relevant bulletin Prot.171598/Z1/12-10-2017",
    art13_sent13_:"In the last link, you can the European Health Insurance Card (EHIC).",
};