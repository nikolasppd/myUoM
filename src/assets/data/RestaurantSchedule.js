export const RESTAURANT_HOURS = {
    for_lunch: {
        on_weekdays: {
            start: {
                hours: 12,
                minutes: 30,
            },
            end: {
                hours: 15,
                minutes: 30,
            },
        },
        on_weekend: {
            start: {
                hours: 13,
                minutes: 0,
            },
            end: {
                hours: 15,
                minutes: 30,
            },
        },
    },
    for_dinner: {
        on_weekdays: {
            start: {
                hours: 18,
                minutes: 0,
            },
            end: {
                hours: 20,
                minutes: 0,
            },
        },
        on_weekend: {
            start: {
                hours: 18,
                minutes: 0,
            },
            end: {
                hours: 20,
                minutes: 0,
            },
        },
    },
    for_breakfast: {
        on_weekdays: {
            start: {
                hours: 8,
                minutes: 0,
            },
            end: {
                hours: 9,
                minutes: 30,
            },
        },
        on_weekend: {
            start: {
                hours: 8,
                minutes: 0,
            },
            end: {
                hours: 9,
                minutes: 30,
            },
        },
    },
};