/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/
import i18n from "../../i18n"
const FirstYearInfo = [
  {
    index: 1,
    tabOrder: 1,
    title: i18n.t("art1_title_"),
    content: [
      i18n.t("art1_sent1_"),
      i18n.t("art1_sent2_"),
      i18n.t("art1_sent3_"),
      i18n.t("art1_sent4_"),
      i18n.t("art1_sent5_"),
      i18n.t("art1_sent6_"),
      i18n.t("art1_sent7_"),
    ],
    links: ["https://eregister.it.minedu.gov.gr/"],
  },

  {
    index: 1,
    tabOrder: 2,
    title: i18n.t("art2_title_"),
    content: [
      i18n.t("art2_sent1_"),
      i18n.t("art2_sent2_"),
      i18n.t("art2_sent3_"),
      i18n.t("art2_sent4_"),
      i18n.t("art2_sent5_"),
      i18n.t("art2_sent6_"),
    ],
    links: ["https://www.uom.gr/register"],
  },
  {
    index: 2,
    tabOrder: 1,
    title: i18n.t("art3_title_"),
    content: [
      i18n.t("art3_sent1_"),
      i18n.t("art3_sent2_"),
      i18n.t("art3_sent3_"),
      i18n.t("art3_sent4_"),
      i18n.t("art3_sent5_"),
      i18n.t("art3_sent6_"),
      i18n.t("art3_sent7_"),
      i18n.t("art3_sent8_"),
      i18n.t("art3_sent9_"),
      i18n.t("art3_sent10_"),

    ],

    links: ["https://www.uom.gr/register", "https://www.photopea.com/"],
  },
  {
    index: 2,
    tabOrder: 2,
    title: i18n.t("art4_title_"),
    content: [
      i18n.t("art4_sent1_"),
      i18n.t("art4_sent2_"),
      i18n.t("art4_sent3_"),
      i18n.t("art4_sent4_"),
      i18n.t("art4_sent5_"),
      i18n.t("art4_sent6_"),
      i18n.t("art4_sent7_"),
      i18n.t("art4_sent8_"),
      i18n.t("art4_sent9_"),
      i18n.t("art4_sent10_"),
      i18n.t("art4_sent11_"),
      i18n.t("art4_sent12_"),
      i18n.t("art4_sent13_"),
    ],
    links: [
      "https://www.uom.gr/site/images/downloads/%CE%A5%CF%80%CE%B5%CF%8D%CE%B8%CF%85%CE%BD%CE%B7%20%CE%94%CE%AE%CE%BB%CF%89%CF%83%CE%B7%20%CE%9C%CE%B7%20%CE%B5%CE%B3%CE%B3%CF%81%CE%B1%CF%86%CE%AE%CF%82%20%CF%83%CE%B5%20%CE%AC%CE%BB%CE%BB%CE%B7%20%CE%A3%CF%87%CE%BF%CE%BB%CE%AE%20%CE%AE%20%CE%A4%CE%BC%CE%AE%CE%BC%CE%B1.pdf",
    ],
  },
  {
    index: 3,
    tabOrder: 1,
    title: i18n.t("art5_title_"),
    content: [
      i18n.t("art5_sent1_"),
      i18n.t("art5_sent2_"),
      i18n.t("art5_sent3_"),
      i18n.t("art5_sent4_"),
      i18n.t("art5_sent5_"),
      i18n.t("art5_sent6_"),
      i18n.t("art5_sent7_"),
      i18n.t("art5_sent8_"),
    ],
    links: ["https://uregister.uom.gr/", "https://mypassword.uom.gr/"],
  },
  {
    index: 3,
    tabOrder: 2,
    title: i18n.t("art6_title_"),
    content: [
      i18n.t("art6_sent1_"),
      i18n.t("art6_sent2_"),
      i18n.t("art6_sent3_"),
    ],
    links: ["https://myaccount.google.com/?pli=1&nlr=1"],
  },
  {
    index: 4,
    tabOrder: 1,
    title: i18n.t("art7_title_"),
    content: [
      i18n.t("art7_sent1_"),
      i18n.t("art7_sent2_"),
      i18n.t("art7_sent3_"),
      i18n.t("art7_sent4_"),
      i18n.t("art7_sent5_"),
      i18n.t("art7_sent6_"),
      i18n.t("art7_sent7_"),
      i18n.t("art7_sent8_"),
      i18n.t("art7_sent9_"),
      i18n.t("art7_sent10_"),
      i18n.t("art7_sent11_"),
      i18n.t("art7_sent12_"),
      i18n.t("art7_sent13_"),
      i18n.t("art7_sent14_"),
      i18n.t("art7_sent15_"),
      i18n.t("rel_links_"),
    ],
    links: [
      "https://academicid.minedu.gov.gr/",
      "https://services.uom.gr/unistudent/login.asp",
    ],
  },
  {
    index: 4,
    tabOrder: 2,
    title: i18n.t("art8_title_"),
    content: [
      i18n.t("art8_sent1_"),
      i18n.t("art8_sent2_"),
      i18n.t("art8_sent3_"),
      i18n.t("art8_sent4_"),
      i18n.t("art8_sent5_"),
      i18n.t("art8_sent6_"),
      i18n.t("art8_sent7_"),
      i18n.t("art8_sent8_"),
      i18n.t("rel_links_"),
    ],
    links: [
      "https://service.eudoxus.gr/student/",
      "https://eudoxus.gr/files/User_Manual_Students.pdf",
    ],
  },
  {
    index: 5,
    tabOrder: 1,
    title: i18n.t("art9_title_"),
    content: [
      i18n.t("art9_sent1_"),
      i18n.t("art9_sent2_"),
      i18n.t("art9_sent3_"),
      i18n.t("rel_links_"),
    ],
    links: [
      "https://www.lib.uom.gr/index.php/el/forms",
      "https://www.lib.uom.gr/index.php/el/new-user?view=form",
    ],
  },
  {
    index: 5,
    tabOrder: 2,
    title: i18n.t("art10_title_"),
    content: [
      i18n.t("art10_sent1_"),
      i18n.t("art10_sent2_"),
      i18n.t("art10_sent3_"),
      i18n.t("art10_sent4_"),
      i18n.t("art10_sent5_"),
      i18n.t("art10_sent6_"),
      i18n.t("art10_sent7_"),
      i18n.t("art10_sent8_"),
      i18n.t("art10_sent9_"),
      i18n.t("rel_links_"),
    ],
    links: ["https://openeclass.uom.gr/index.php?logout=yes"],
  },
  {
    index: 6,
    tabOrder: 1,
    title: i18n.t("art11_title_"),
    content: [
      i18n.t("art11_sent1_"),
      i18n.t("art11_sent2_"),
      i18n.t("art11_sent3_"),
      i18n.t("art11_sent4_"),
      i18n.t("art11_sent5_"),
      i18n.t("rel_links_"),
    ],
    links: [
      "https://www.uom.gr/student-care/sitish",
      "https://www.uom.gr/8883-anakoinosh-sitish-protoeton-foithton-akad-etoys-2020-2021",
      "https://www.uom.gr/8576-anakoinosh-sitish-foithton-akad-etoys-2020-2021",
      "https://www.uom.gr/assets/site/public/nodes/8576/7726-4618-dikaiologitika-sitisi-new-2020-21.docx",
      "https://www.uom.gr/assets/site/public/nodes/8576/7706-ypefthini-dilosi-sitisi-2020-2021.doc",
      "https://www.uom.gr/assets/site/public/nodes/8576/7689-2413-fek-1965-18-06-2012-b.pdf",
      "https://www.uom.gr/assets/site/public/nodes/8576/7776-7704-3886-oria-sitisis-2020-2021.docx",
    ],
  },
  {
    index: 6,
    tabOrder: 2,
    title: i18n.t("art12_title_"),
    content: [
      i18n.t("art12_sent1_"),
      i18n.t("art12_sent2_"),
      i18n.t("art12_sent3_"),
      i18n.t("rel_links_"),
    ],
    links: [
      "https://www.uom.gr/8577-anakoinosh-stegash-foithton-akad-etoys-2020-2021",
      "https://www.uom.gr/assets/site/public/nodes/8577/7691-2414-kanonismos-estias-2012-fek.pdf",
      "https://www.uom.gr/assets/site/public/nodes/8577/7737-ypefthini-dilosi-estia.doc",
      "https://www.uom.gr/assets/site/public/nodes/8577/7731-7697-dikaiologitika-stegasi-new-2020-21.docx",
    ],
  },
  {
    index: 6,
    tabOrder: 3,
    title: i18n.t("art13_title_"),
    content: [
      i18n.t("art13_sent1_"),
      i18n.t("art13_sent2_"),
      i18n.t("art13_sent3_"),
      i18n.t("art13_sent4_"),
      i18n.t("art13_sent5_"),
      i18n.t("art13_sent6_"),
      i18n.t("art13_sent7_"),
      i18n.t("art13_sent8_"),
      i18n.t("art13_sent9_"),
      i18n.t("art13_sent10_"),
      i18n.t("art13_sent11_"),
      i18n.t("art13_sent12_"),
      i18n.t("art13_sent13_"),
      i18n.t("rel_links_"),
    ],
    links: [
      "https://www.uom.gr/student-care/ygeionomikh-perithalpsh",
      "https://www.uom.gr/assets/site/public/nodes/5457/2415-img06112017_0001.pdf",
      "https://www.uom.gr/student-care/eyropaikh-karta-asfalishs-astheneias-e-k-a-a",
    ],
  },
];
export default FirstYearInfo;
